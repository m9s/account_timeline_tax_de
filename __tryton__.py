# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Account Timeline Taxes for Germany',
    'name_de_DE': 'Buchhaltung Gültigkeitsdauer Steuern für Deutschland',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''Tax Data for Germany
    Provides data for german
    - taxes
    - product tax types
    - tax groups
''',
    'description_de_DE': '''Steuerdaten für Deutschland
    Fügt folgende Daten für Deutschland hinzu:
    - Steuern
    - Steuertypen für Artikel
    - Steuergruppen
''',
    'depends': [
        'account_timeline',
        'account_timeline_invoice',
        'account_invoice_tax_rule_product_type',
        'account_tax_recapitulative_statement'
    ],
    'xml': [
        'account_timeline_tax_de.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
